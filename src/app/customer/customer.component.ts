import { Component, OnInit } from '@angular/core';
import {Customer} from "../interfaces/customer";
import {CustomerService} from "../services/customer.service";

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  customers : Customer[];
  customer: any;
  constructor( private customerService : CustomerService) {
    this.getCustomers();
  }

  getCustomers(){
    this.customerService.get().subscribe((data: Customer[])=>{
      this.customers = data;
    }, (error)=>{
      console.log(error);
    });
  }

  delete(id){
    this.customer = this.customers.find((c)=>{
      return c.id == id;
    });
    if (confirm('Desea eliminar a ' + this.customer.name)){
      this.customerService.delete(id).subscribe(()=>{
        alert("Eliminado con exito");
        this.getCustomers();
      }, (error)=>{
        alert("Orror al eliminar");
      });
    }
  }

  ngOnInit() {
  }

}
