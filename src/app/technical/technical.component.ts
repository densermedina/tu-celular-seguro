import { Component, OnInit } from '@angular/core';
import {Technical} from "../interfaces/technical";
import {TechnicalService} from "../services/technical.service";

@Component({
  selector: 'app-technical',
  templateUrl: './technical.component.html',
  styleUrls: ['./technical.component.css']
})
export class TechnicalComponent implements OnInit {
  technicals : Technical[];
  technical:any;
  constructor(private technicalService: TechnicalService) {
    this.getTechnicals();
  }

  getTechnicals(){
    this.technicalService.get().subscribe((data: Technical[])=>{
      this.technicals = data;
      console.log('Datos obtenidos con exito' + data);
    }, (error)=>{
      console.log(error);
      alert("Error al obtener datos");
    });

  }

  delete(id){
    this.technical = this.technicals.find((t)=>{
      return t.id == id;
    });
    if (confirm('Desea eliminar a ' + this.technical.name)){
      this.technicalService.delete(id).subscribe(()=>{
        alert("Eliminado con exito");
        console.log("elminiado");
        this.getTechnicals();
      }, (error)=>{
        console.log(error);
        console.log("error al elminiar");
        alert("Orror al eliminar");
      });
    }
  }

  ngOnInit() {
  }

}
