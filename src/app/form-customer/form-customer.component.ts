import { Component, OnInit } from '@angular/core';
import {CustomerService} from "../services/customer.service";
import {Customer} from "../interfaces/customer";
import {Technical} from "../interfaces/technical";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-form-customer',
  templateUrl: './form-customer.component.html',
  styleUrls: ['./form-customer.component.css']
})
export class FormCustomerComponent implements OnInit {
  customer: Customer = {
    dni: null,
    name: null,
    phone_1: null,
    phone_2: null,
  };
  id: any;
  editing: boolean = false;
  customers : Customer[];

  constructor( private customerService : CustomerService,
               private activatedRoute: ActivatedRoute,
              public router: Router) {
    this.id = this.activatedRoute.snapshot.params['id'];
    console.log("Rutas");
    console.log(this.id);
    if(this.id){
      this.editing = true;
      this.customerService.get().subscribe((data: Customer[])=>{
        this.customers = data;
        this.customer = this.customers.find((t)=>{
          return t.id == this.id
        });
      }, (error)=>{
        console.log(error);
      });
    }else {
      this.editing = false;
    }

  }

  saveCustomer(){

    if (this.editing){
      this.customerService.put(this.customer).subscribe((data)=>{
        // alert("Cliente Actualizado");
        // this.router.navigateByUrl('/customer');
      },(error)=>{
        alert("Ocurrio un orror");
        this.router.navigateByUrl('/customer');
      });
    } else {
      this.customerService.post(this.customer).subscribe((data)=>{
        // alert("Cliente Creado");
        // this.router.navigateByUrl('/customer');
        console.log("Data devuelto de Customer");
        console.log(data);
      },(error)=>{
        alert("Ocurrio un orror");
        this.router.navigateByUrl('/customer');
      });
    }
  }



  ngOnInit() {
  }

}
