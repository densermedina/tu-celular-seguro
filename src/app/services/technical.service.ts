import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Technical} from "../interfaces/technical";

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TechnicalService {
  constructor(private httpClient : HttpClient) { }

  API_ENDPOINT = 'http://127.0.0.1:8000/api';

  // httpOptions = {
  //   headers: new HttpHeaders({
  //     'Content-Type': 'application/json'
  //   })
  // }

  // Error handling
  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  // Obtener Datos del API
  get(){
    const headers = new HttpHeaders({'Content-type': 'application/json'});
    return this.httpClient.get(this.API_ENDPOINT + '/technical',{headers:headers});
  }


  // get(): Observable<Technical> {
  //   return this.httpClient.get<Technical>(this.API_ENDPOINT + '/technical')
  //     .pipe(
  //       retry(1),
  //       catchError(this.handleError)
  //     )
  // }


  // Esto es el metodo que guarda en el API
  post(technical: Technical){
    // Para todos los metodos menos GET crear HEADERS
    const headers = new HttpHeaders({'Content-type': 'application/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/technical', technical, {headers:headers});
  }

  put(technical){
    const headers = new HttpHeaders({'Content-type': 'application/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/technical/' + technical.id , technical, {headers:headers});
  }

  delete(id){
    return this.httpClient.delete(this.API_ENDPOINT + '/technical/' + id);
  }
}
