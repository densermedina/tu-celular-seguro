import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Customer} from "../interfaces/customer";
import {Agreement} from "../interfaces/agreement";

@Injectable({
  providedIn: 'root'
})
export class AgreementService {

  constructor(private  httpClient:HttpClient) { }

  API_ENDPOINT = 'http://127.0.0.1:8000/api';


  get(){
    const headers = new HttpHeaders({'Content-type' : 'aplication/json'});
    return this.httpClient.get(this.API_ENDPOINT + '/agreement',{headers:headers});
  }

  post(agreement : Agreement){
    const headers = new HttpHeaders({'Content-type' : 'aplication/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/agreement', agreement, {headers :  headers});
  }

  put(agreement){
    const headers = new HttpHeaders({'Content-type': 'application/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/agreement/' + agreement.id , agreement, {headers:headers});
  }

  delete(id){
    return this.httpClient.delete(this.API_ENDPOINT + '/agreement/' + id);
  }

}


