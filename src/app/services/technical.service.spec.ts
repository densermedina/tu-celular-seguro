import { TestBed } from '@angular/core/testing';

import { TechnicalService } from './technical.service';

describe('TechnicalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TechnicalService = TestBed.get(TechnicalService);
    expect(service).toBeTruthy();
  });
});
