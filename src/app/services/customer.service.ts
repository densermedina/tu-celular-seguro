import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Customer} from "../interfaces/customer";

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  API_ENDPOINT = 'https://laravel.celularseguros.com/api';


  constructor(private httpClient : HttpClient) { }

  get(){
    const headers = new HttpHeaders({'Content-type' : 'aplication/json'})
    return this.httpClient.get(this.API_ENDPOINT + '/customer', {headers :  headers});
  }

  post(customer : Customer){
    const headers = new HttpHeaders({'Content-type' : 'aplication/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/customer', customer, {headers :  headers});
  }

  put(customer){
    const headers = new HttpHeaders({'Content-type': 'application/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/customer/' + customer.id , customer, {headers:headers});
  }

  delete(id){
    return this.httpClient.delete(this.API_ENDPOINT + '/customer/' + id);
  }


}
