export interface Technical {
  id? : number;
  dni: string;
  name: string;
  last_name: string;
  phone?: string;
  created_ad? : string;
  update_ad? : string;
}
