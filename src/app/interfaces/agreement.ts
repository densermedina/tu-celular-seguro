export interface Agreement {
  id? : number;
  id_technical: number;
  id_customer: number;
  code_ticket: string;
  customer_description: string;
  technical_description: string;
  model: string;
  price: number;
  created_ad? : string;
  update_ad? : string;
}
