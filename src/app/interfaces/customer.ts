export interface Customer {
  id? : number;
  dni: string;
  name: string;
  phone_1?: string;
  phone_2?: string;
  created_ad? : string;
  update_ad? : string;
}
