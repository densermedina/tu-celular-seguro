import { Component, OnInit } from '@angular/core';
import {Technical} from "../interfaces/technical";
import {TechnicalService} from "../services/technical.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-form-technical',
  templateUrl: './form-technical.component.html',
  styleUrls: ['./form-technical.component.css']
})
export class FormTechnicalComponent implements OnInit {
  technical : Technical = {
    dni : null,
    name : null,
    last_name : null,
    phone : null,
  };
  //Declaramos de manera global id
  id: any;
  editing: boolean = false;
  technicals : Technical[];
  constructor(private technicalService: TechnicalService,
              private activatedRoute: ActivatedRoute,
              public router: Router) {
    //capturar el id , id = id de la configuracion de la ruta en app.module.ts
    this.id = this.activatedRoute.snapshot.params['id'];
    if(this.id){
      this.editing = true;
      this.technicalService.get().subscribe((data: Technical[])=>{
        this.technicals = data;
        this.technical = this.technicals.find((t)=>{
          return t.id == this.id
        });
        console.log(this.technical);
      }, (error)=>{
        console.log(error);
      });
    }else {
      this.editing = false;
    }
  }

  ngOnInit() {
  }
  saveTechnical(){

    if (this.editing){
      this.technicalService.put(this.technical).subscribe((data)=>{
        alert("Tecnico Actualizado");
        this.router.navigateByUrl('/technical');
      },(error)=>{
        alert("Ocurrio un orror");
        this.router.navigateByUrl('/technical');
      });
    } else {
      this.technicalService.post(this.technical).subscribe((data)=>{
        alert("Tecnico guardado");
        this.router.navigateByUrl('/technical');
      },(error)=>{
        alert("Ocurrio un orror");
        this.router.navigateByUrl('/technical');

      });
    }
  }

}
