import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTechnicalComponent } from './form-technical.component';

describe('FormTechnicalComponent', () => {
  let component: FormTechnicalComponent;
  let fixture: ComponentFixture<FormTechnicalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormTechnicalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTechnicalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
