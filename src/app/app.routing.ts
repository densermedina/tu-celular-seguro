import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './home/home.component';

const appRoutes = [
  {
    path: '',
    component: HomeComponent,  pathMatch: 'full'
  }
];
export const appRoutesPage = RouterModule.forRoot(appRoutes);
