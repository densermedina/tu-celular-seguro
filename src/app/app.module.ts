import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { appRoutesPage } from './app.routing';
import { PageModule } from './page/page.module';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { TechnicalComponent } from './technical/technical.component';
import {Route, RouterModule} from "@angular/router";
import {HttpClientModule} from '@angular/common/http';
import { FormTechnicalComponent } from './form-technical/form-technical.component';
import {FormsModule} from "@angular/forms";
import { CustomerComponent } from './customer/customer.component';
import { FormCustomerComponent } from './form-customer/form-customer.component';
import { AgreementComponent } from './agreement/agreement.component';
import {FormAgreementComponent} from "./form-agreement/form-agreement.component";
import { DashboardComponent } from './dashboard/dashboard.component';
import { PartsComponent } from './parts/parts.component';
import { MainComponent } from './page/main/main.component';

//CREANDO RUTAS
const routes: Route[] = [
  {path: '', component: MainComponent},
  {path: 'main', component: MainComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'technical', component: TechnicalComponent},
  {path: 'form-technical', component: FormTechnicalComponent},
  {path: 'form-technical/:id', component: FormTechnicalComponent},
  {path: 'customer', component: CustomerComponent},
  {path: 'form-customer', component: FormCustomerComponent},
  {path: 'form-customer/:id', component: FormCustomerComponent},
  {path: 'agreement', component: AgreementComponent},
  {path: 'form-agreement', component: FormAgreementComponent},
  {path: 'form-agreement/:id', component: FormAgreementComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TechnicalComponent,
    FormTechnicalComponent,
    CustomerComponent,
    FormCustomerComponent,
    AgreementComponent,
    FormAgreementComponent,
    DashboardComponent,
    PartsComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
    PageModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
