import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';

import { HeaderComponent } from './header/header.component';
import { PartsComponent } from './parts/parts.component';
import { BlogComponent } from './blog/blog.component';
import { ServicesComponent } from './services/services.component';
import { QuoteComponent } from './quote/quote.component';
import { HomeComponent } from './home/home.component';


const secondaryRoutes: Routes = [
  {
    path: 'main',
    component: MainComponent,
    children:[
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'services',
        component: ServicesComponent
      },
      {
        path: 'blog',
        component: BlogComponent
      },
      {
        path: 'parts',
        component: PartsComponent
      },
      {
        path: 'quote',
        component: QuoteComponent
      },


    ]
  },
];

//taken from angular.io
//Only call RouterModule.forRoot in the root AppRoutingModule (or the AppModule if
//that's where you register top level application routes). In any other module, you
//must call the RouterModule.forChild method to register additional routes.

@NgModule({
  imports: [
    RouterModule.forChild(secondaryRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class SecondaryRoutingModule { }
