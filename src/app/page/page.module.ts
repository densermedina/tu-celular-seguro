import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import {Route, RouterModule} from "@angular/router";
import { PartsComponent } from './parts/parts.component';
import { BlogComponent } from './blog/blog.component';
import { ServicesComponent } from './services/services.component';
import { QuoteComponent } from './quote/quote.component';
import { HomeComponent } from './home/home.component';
import { SecondaryRoutingModule } from './page.routing'

const routeX: Route[] = [
];

@NgModule({
  declarations: [
    HeaderComponent,
    MainComponent,
    PartsComponent,
    BlogComponent,
    ServicesComponent,
    QuoteComponent,
    HomeComponent
  ],
  exports:[
    HeaderComponent,
    MainComponent,

  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(routeX),
    SecondaryRoutingModule
  ]
})
export class PageModule { }
