import { Component, OnInit } from '@angular/core';
import {AgreementService} from "../services/agreement.service";
import {Agreement} from "../interfaces/agreement";

@Component({
  selector: 'app-agreement',
  templateUrl: './agreement.component.html',
  styleUrls: ['./agreement.component.css']
})
export class AgreementComponent implements OnInit {

  constructor( private agreementService : AgreementService) {
    this.getAgreemet();
  }

  agreements: Agreement[];
  agreement:any;
  getAgreemet(){
    this.agreementService.get().subscribe((data: Agreement[])=>{
      this.agreements = data;
      console.log("agreement get successful");
    }, (error)=>{
      console.log(error);
    });
  }

  delete(id){
    this.agreement = this.agreements.find((a)=>{
      return a.id == id;
    });
    if (confirm('Desea eliminar a ' + this.agreement.customer.name)){
      this.agreementService.delete(id).subscribe(()=>{
        alert("Eliminado con exito");
        console.log("elminiado");
        this.getAgreemet();
      }, (error)=>{
        console.log(error);
        console.log("error al elminiar");
        alert("Orror al eliminar");
      });
    }
  }


  ngOnInit() {
  }

}
