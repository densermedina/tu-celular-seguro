import { Component, OnInit } from '@angular/core';
import {Technical} from "../interfaces/technical";
import {TechnicalService} from "../services/technical.service";
import {CustomerService} from "../services/customer.service";
import {Customer} from "../interfaces/customer";
import {Agreement} from "../interfaces/agreement";
import {AgreementService} from "../services/agreement.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-form-agreement',
  templateUrl: './form-agreement.component.html',
  styleUrls: ['./form-agreement.component.css']
})
export class FormAgreementComponent implements OnInit {
  technicals : Technical[];
  technical: Technical = {
    dni:null,
    name:null,
    last_name:null,
    phone: null
  };
  customers : Customer[];
  customerData: any;
  customer: Customer = {
    id:null,
    dni: null,
    name: null,
    phone_1: null,
    phone_2: null,
  };
  agreements: Agreement[];
  agreement: Agreement={
    id_technical: null,
    id_customer: null,
    code_ticket: null,
    customer_description: null,
    technical_description: null,
    model: null,
    price: null
  };
  id: any;
  editing: boolean = false;

  constructor(private technicalService: TechnicalService,
              private customerService: CustomerService,
              private agreementService: AgreementService,
              private activatedRoute: ActivatedRoute,
              public router: Router) {
    this.getTechnicals();
    this.id = this.activatedRoute.snapshot.params['id'];
    if(this.id){
      this.editing = true;
      this.agreementService.get().subscribe((data: Agreement[])=>{
        this.agreements = data;
        this.agreement = this.agreements.find((t)=>{
          return t.id == this.id
        });
      }, (error)=>{
        console.log(error);
      });
    }else {
      this.editing = false;
    }

  }

//=======================================================================================================
  getTechnicals(){
    this.technicalService.get().subscribe((data: Technical[])=>{
      this.technicals = data;
      console.log("Tecnicos obtenidos correctamente");
    }, (error)=>{
      console.log(error);
      alert("Error al obtener datos");
    });
  }
  //=======================================================================================================


  saveAgreement(){
    if (this.editing){
      this.agreementService.put(this.agreement).subscribe((data)=>{
        alert("Reparacion Actualizada");
        // this.router.navigateByUrl('/agreement');
      },(error)=>{
        alert("Ocurrio un orror");
        // this.router.navigateByUrl('/agreement');
      });
    } else {
      this.customerService.post(this.customer).subscribe((data)=>{
        this.customerData = data;
        this.agreement.id_customer = this.customerData.id.toString();

        this.agreementService.post(this.agreement).subscribe((data)=>{
          alert("Contrato agregado correctamente");
          // this.router.navigateByUrl('/agreement');
        },(error)=>{
          alert("Ocurrio un orror de AGREEMENT");
          console.log("Error de Agreement");
          console.log(error);
          // this.router.navigateByUrl('/agreement');
        });
      }, (error) => {
        console.log(error);
        alert("Error al guardar cliente")
      });


      // this.customerService.post(this.customer).subscribe((data)=>{
      //   console.log("Datos de Customer");
      //   console.log(data);
      // }, (error)=>{
      //   alert("Ocurrio un orror");
      //   console.log(error);
      // });
    }
  }


  ngOnInit() {
  }

}
